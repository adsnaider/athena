//! Architecture specific utilities.

#[cfg(target_arch = "x86_64")]
pub mod x86_64_utils;
